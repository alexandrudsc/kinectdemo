﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KinectClient
{
    public partial class BitmapForm : Form
    {
        private KinectClient.BodyPosture posture;
        public BitmapForm(KinectClient.BodyPosture posture)
        {
            InitializeComponent();
            this.posture = posture;
        }

        private void BitmapForm_Load(object sender, EventArgs e)
        {
            pbSkeleton.Image = new Bitmap(pbSkeleton.Width, pbSkeleton.Height);
            using (Bitmap bitmap = posture.ToBitmap(640, 480))
            using (Graphics graphics = Graphics.FromImage(pbSkeleton.Image))
            {
                graphics.DrawImage(bitmap, 0, 0);
                pbSkeleton.Refresh();
            }
        }
    }
}
