﻿using System.Drawing;

namespace KinectClient
{
    public class Point3D
    {
        public float X, Y, Z;
        public Point ScreenPoint;
        public string Type;
        
        public override string ToString()
        {
            System.Text.StringBuilder stringBuilder = new System.Text.StringBuilder();
            stringBuilder.Append(string.Format("({0:N2}, {1:N2}, {2:N2})", X, Y, Z));
            return stringBuilder.ToString();
        }

    }
}
