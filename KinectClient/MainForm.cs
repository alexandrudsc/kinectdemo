﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading;
using System.Windows.Forms;
using System.Net;
using System.Net.Sockets;
using System.IO;

namespace KinectClient
{
    public partial class MainForm : Form
    {
        Thread thread = null;
        private volatile BodyPosture currentBodyPosture;

        public MainForm()
        {
            InitializeComponent();

            pbSkeleton.Image = new Bitmap(pbSkeleton.Width, pbSkeleton.Height);

            thread = new Thread(new ThreadStart(GetData));
            thread.Start();
        }

        private void GetData()
        {
            while (1 == 1)
            {
                try
                {
                    TcpClient client = new TcpClient();
                    client.Connect("172.20.122.25", 12345);
                    NetworkStream stream = client.GetStream();
                    byte[] buffer = new byte[1024 * 1024];
                    int numBytesRead = stream.Read(buffer, 0, buffer.Length);

                    currentBodyPosture = new BodyPosture(
                        System.Text.ASCIIEncoding.ASCII.GetString(
                          buffer, 0, numBytesRead)
                    );
                    UpdateBodyPosture(currentBodyPosture);
                    client.Close();

                    Thread.Sleep(500);
                }
                catch (Exception exc)
                {
                    Console.WriteLine(exc.ToString());
                }
            }
        }

        private delegate void UpdateBodyPostureDelegate(BodyPosture posture);
        private void UpdateBodyPosture(BodyPosture posture)
        {
            if (this.InvokeRequired)
            {
                this.Invoke(new UpdateBodyPostureDelegate(UpdateBodyPosture), new object[] { posture });
                return;
            }

            using(Bitmap bitmap = posture.ToBitmap(640, 480))
            using (Graphics graphics = Graphics.FromImage(pbSkeleton.Image))
            {
                graphics.DrawImage(bitmap, 0, 0);
                pbSkeleton.Refresh();
            }
        }

        private void Form1_FormClosing(object sender, FormClosingEventArgs e)
        {
            if (thread != null)
            {
                try
                {
                    thread.Abort();
                }
                catch { }
            }
        }

        private void toolStripBtnSaveBodyPosture_Click(object sender, EventArgs e)
        {
            InputDialog inputDialog = new InputDialog("Postura");
            BodyPosture posture = currentBodyPosture;
            inputDialog.ShowDialog();
            string filename = inputDialog.returnValue1 + ".txt";
            if (posture == null)
                return;
            System.IO.File.WriteAllText(filename, posture.ToString());
        }

        private void toolStripBtnExit_Click(object sender, EventArgs e)
        {
            this.Close();
            Environment.Exit(0);
        }

        private void toolStripBtnShowPosture_Click(object sender, EventArgs e)
        {
            OpenFileDialog fileDialog = new OpenFileDialog();
            fileDialog.ShowDialog();
            string filename = fileDialog.FileName;
            if (filename == null || filename == String.Empty)
            {
                return;
            }
            string posture_str = System.IO.File.ReadAllText(filename);
            BodyPosture loaded_posture = new BodyPosture(posture_str);
            currentBodyPosture = loaded_posture;
            BitmapForm bitmapForm = new BitmapForm(loaded_posture);
            bitmapForm.ShowDialog();
        }

        private void toolStripBtnTrain_Click(object sender, EventArgs e)
        {
            learning.KNN.init(4);
            this.folderBrowserDialog.ShowDialog();
            string folder_path = this.folderBrowserDialog.SelectedPath;
            if (folder_path == null || folder_path == String.Empty)
            {
                return;
            }

            learning.Model model = new learning.Model();
            foreach (string file in Directory.GetFiles(folder_path, "*-*.txt"))
            {
                string contents = File.ReadAllText(file);
                BodyPosture bp = new BodyPosture(contents);
                model.add_posture(bp.Points, file);
            }
            learning.KNN.train(model);
        }

        private void toolStripButtonClassify_Click(object sender, EventArgs e)
        {
            if (currentBodyPosture == null)
            {
                return;
            }
            BodyPosture bp = currentBodyPosture;
            learning.Model.InputOutput inout = new learning.Model.InputOutput();
            try
            {
                StringBuilder sb = new StringBuilder();
                foreach (learning.Model.BodyPart body_part in Enum.GetValues(typeof(learning.Model.BodyPart)))
                {
                    inout.body_part = body_part;
                    inout.input = bp[learning.Model.BodyPartToString(inout.body_part)];
                    int result = learning.KNN.classify(inout);
                    sb.Append(learning.Model.BodyPartToString(inout.body_part));
                    sb.Append("-> ").Append(result == (int)learning.Model.Positions.UP ? "Up" : "Down");
                    sb.Append(Environment.NewLine);
                }
                MessageBox.Show(sb.ToString());
            }
            catch (Exception ex)
            {
                MessageBox.Show(ex.Message);
            }

        }
    }
}
