﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace KinectClient
{
    public partial class InputDialog : Form
    {
        public string returnValue1;
        public InputDialog(string lblName)
        {
            InitializeComponent();
            this.lbl1.Text = lblName;
        }

        private void btnOk_Click(object sender, EventArgs e)
        {
            this.returnValue1 = this.textBox1.Text;
            this.DialogResult =  DialogResult.OK;
            this.Close();
        }
    }
}
