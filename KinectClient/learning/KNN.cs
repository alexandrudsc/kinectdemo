﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

using Accord.MachineLearning;

namespace KinectClient.learning
{
    class KNN
    {
        private static Dictionary<Model.BodyPart, KNearestNeighbors> knns = null;
        private static bool trained = false;
        public static void init( int n )
        {
            knns = new Dictionary<Model.BodyPart, KNearestNeighbors>();
            foreach (Model.BodyPart body_part in Enum.GetValues(typeof(Model.BodyPart)))
            {
                knns.Add(body_part, new KNearestNeighbors(k: n));
            }
        }

        public static void train(Model model)
        {
            if (knns == null)
            {
                throw new Exception("algorithm not initialized");
            }
            Dictionary<Model.BodyPart, List<double[]> > input = new Dictionary<Model.BodyPart, List<double[]>>();
            Dictionary<Model.BodyPart, List<int>> output = new Dictionary<Model.BodyPart, List<int>>();
            foreach (Model.BodyPart body_part in Enum.GetValues(typeof(Model.BodyPart)))
            {
                input.Add(body_part, new List<double[]>());
                output.Add(body_part, new List<int>());
            }
            foreach (Model.InputOutput inout in model.inputs)
            {
                input[inout.body_part].Add(inout.input);
                output[inout.body_part].Add(inout.output);
            }
            foreach (Model.BodyPart body_part in Enum.GetValues(typeof(Model.BodyPart)))
            {
                knns[body_part].Learn(input[body_part].ToArray(), output[body_part].ToArray());
            }
            trained = true;
        }

        public static int classify(Model.InputOutput inout)
        {
            if (!trained)
            {
                throw new Exception("Knn not trained");
            }
            return knns[inout.body_part].Decide(inout.input);
        }
    }
}
