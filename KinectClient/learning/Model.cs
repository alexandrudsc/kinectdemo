﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace KinectClient.learning
{
    class Model
    {
        private readonly int MAX_ELEM = 100;
        public enum BodyPart
        {
            LEFT_HAND = 0,
            RIGHT_HAND = 1,
            LEFT_FOOT = 2,
            RIGHT_FOOT = 3
        }

        public static string BodyPartToString(BodyPart body_part)
        {
            switch (body_part)
            {
                case BodyPart.LEFT_FOOT:
                    return "FootLeft";
                case BodyPart.RIGHT_FOOT:
                    return "FootRight";
                case BodyPart.LEFT_HAND:
                    return "HandLeft";
                case BodyPart.RIGHT_HAND:
                    return "HandRight";
                default:
                    return "";
            }
        }

        public enum Positions
        { 
            DOWN = 0,
            UP = 1,
        }

        public struct InputOutput
        {
            public double[] input;
            public int output;
            public BodyPart body_part;

            public InputOutput(double[] input, int output, BodyPart body_part)
            {
                this.input = input;
                this.output = output;
                this.body_part = body_part;
            }
        }

        public List< InputOutput> inputs = new List<InputOutput>();
        public List<int> outputs = null;

        public Model()
        {
            outputs = new List<int>(MAX_ELEM);
        }

        public void add_posture(List<Point3D> posture, string label)
        {
            if (posture == null || label == null)
            {
                return;
            }
            foreach(Point3D point in posture)
            {
                var coords = new double[]{ point.X, point.Y, point.Z };

                BodyPart part;
                switch (point.Type)
                {
                    case "HandLeft":
                        part = BodyPart.LEFT_HAND;
                        break;
                    case "HandRight":
                        part = BodyPart.RIGHT_HAND;
                        break;
                    case "FootLeft":
                        part = BodyPart.LEFT_FOOT;
                        break;
                    case "FootRight":
                        part = BodyPart.RIGHT_FOOT;
                        break;
                    default:
                        continue;
                }

                Positions position = Positions.DOWN;
                if (label.Contains("stanga") && part == BodyPart.LEFT_HAND)
                    position = Positions.UP;
                else if (label.Contains("dreapta") && part == BodyPart.RIGHT_HAND)
                    position = Positions.UP;
                else if (label.Contains("stang") && part == BodyPart.LEFT_FOOT)
                    position = Positions.UP;
                else if (label.Contains("drept") && part == BodyPart.RIGHT_FOOT)
                    position = Positions.UP;

                inputs.Add(new InputOutput(coords, (int)position, part));

                outputs.Add((int)position);
            }
        }
    }
}
