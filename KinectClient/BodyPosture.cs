﻿using System;
using System.Collections.Generic;
using System.Drawing;

namespace KinectClient
{
    public class BodyPosture
    {
        public List<Point3D> Points;

        public BodyPosture()
        {
            Points = new List<Point3D>();
        }

        public BodyPosture(string text)
        {
            Points = new List<Point3D>();
            string[] values = text.Split(';');
            for (int i = 0; i < values.Length - 1; i++)
            {
                string[] coordinates = values[i].Split(',');
                Points.Add(new Point3D()
                {
                    X = float.Parse(coordinates[0]),
                    Y = float.Parse(coordinates[1]),
                    Z = float.Parse(coordinates[2]),
                    ScreenPoint = new Point(int.Parse(coordinates[3]), int.Parse(coordinates[4])),
                    Type = coordinates[5]
                });
            }
        }

        public Bitmap ToBitmap(int width, int height)
        {
            Bitmap bitmap = new Bitmap(width, height);
            using (Graphics graphics = Graphics.FromImage(bitmap))
            {
                graphics.Clear(Color.White);
                foreach (Point3D point in Points)
                {
                    int x = point.ScreenPoint.X;
                    int y = point.ScreenPoint.Y;
                    int R = 5;
                    SolidBrush brush = null;
                    switch (point.Type)
                    {
                        case "Head": brush = new SolidBrush(Color.Red); R = 10;  break;
                        case "HandLeft": brush = new SolidBrush(Color.Green); R = 10;  break;
                        case "HandRight": brush = new SolidBrush(Color.Blue); R = 10;  break;
                        default: brush = new SolidBrush(Color.Black); R = 5;  break;
                    }
                    graphics.FillEllipse(
                        brush, 
                        new Rectangle(x - R, y - R, 2 * R, 2 * R));
                    brush.Dispose();
                }
            }
            return bitmap;
        }

        public double[] this[string key]
        {
            get
            {
                double[] coords = null;
                foreach (Point3D point in Points)
                {
                    if (point.Type == key)
                    {
                        coords = new double[] { point.X, point.Y, point.Z };
                    }
                }
                return coords;
            }
            set
            {
            }
        }

        public override string ToString()
        {
            string text = "";
            foreach (Point3D point in Points)
            {
                text += String.Format("{0:.000},{1:.000},{2:.000},{3},{4},{5};",
                    point.X, point.Y, point.Z,
                    point.ScreenPoint.X, point.ScreenPoint.Y,
                    point.Type
                );
            }
            return text;
        }

    }
}
