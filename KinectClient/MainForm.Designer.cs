﻿namespace KinectClient
{
    partial class MainForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(MainForm));
            this.pbSkeleton = new System.Windows.Forms.PictureBox();
            this.toolStrip1 = new System.Windows.Forms.ToolStrip();
            this.toolStripBtnSaveBodyPosture = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator1 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBtnExit = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator2 = new System.Windows.Forms.ToolStripSeparator();
            this.toolStripBtnShowPosture = new System.Windows.Forms.ToolStripButton();
            this.toolStripBtnTrain = new System.Windows.Forms.ToolStripButton();
            this.toolStripSeparator3 = new System.Windows.Forms.ToolStripSeparator();
            this.folderBrowserDialog = new System.Windows.Forms.FolderBrowserDialog();
            this.toolStripButtonClassify = new System.Windows.Forms.ToolStripButton();
            ((System.ComponentModel.ISupportInitialize)(this.pbSkeleton)).BeginInit();
            this.toolStrip1.SuspendLayout();
            this.SuspendLayout();
            // 
            // pbSkeleton
            // 
            this.pbSkeleton.Location = new System.Drawing.Point(12, 30);
            this.pbSkeleton.Name = "pbSkeleton";
            this.pbSkeleton.Size = new System.Drawing.Size(640, 462);
            this.pbSkeleton.TabIndex = 0;
            this.pbSkeleton.TabStop = false;
            // 
            // toolStrip1
            // 
            this.toolStrip1.Items.AddRange(new System.Windows.Forms.ToolStripItem[] {
            this.toolStripBtnSaveBodyPosture,
            this.toolStripSeparator1,
            this.toolStripBtnExit,
            this.toolStripSeparator2,
            this.toolStripBtnShowPosture,
            this.toolStripBtnTrain,
            this.toolStripSeparator3,
            this.toolStripButtonClassify});
            this.toolStrip1.Location = new System.Drawing.Point(0, 0);
            this.toolStrip1.Name = "toolStrip1";
            this.toolStrip1.Size = new System.Drawing.Size(663, 25);
            this.toolStrip1.TabIndex = 1;
            this.toolStrip1.Text = "toolStrip1";
            // 
            // toolStripBtnSaveBodyPosture
            // 
            this.toolStripBtnSaveBodyPosture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripBtnSaveBodyPosture.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnSaveBodyPosture.Image")));
            this.toolStripBtnSaveBodyPosture.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnSaveBodyPosture.Name = "toolStripBtnSaveBodyPosture";
            this.toolStripBtnSaveBodyPosture.Size = new System.Drawing.Size(98, 22);
            this.toolStripBtnSaveBodyPosture.Text = "Salveaza postura";
            this.toolStripBtnSaveBodyPosture.Click += new System.EventHandler(this.toolStripBtnSaveBodyPosture_Click);
            // 
            // toolStripSeparator1
            // 
            this.toolStripSeparator1.Name = "toolStripSeparator1";
            this.toolStripSeparator1.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripBtnExit
            // 
            this.toolStripBtnExit.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripBtnExit.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnExit.Image")));
            this.toolStripBtnExit.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnExit.Name = "toolStripBtnExit";
            this.toolStripBtnExit.Size = new System.Drawing.Size(29, 22);
            this.toolStripBtnExit.Text = "Exit";
            this.toolStripBtnExit.Click += new System.EventHandler(this.toolStripBtnExit_Click);
            // 
            // toolStripSeparator2
            // 
            this.toolStripSeparator2.Name = "toolStripSeparator2";
            this.toolStripSeparator2.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripBtnShowPosture
            // 
            this.toolStripBtnShowPosture.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripBtnShowPosture.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnShowPosture.Image")));
            this.toolStripBtnShowPosture.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnShowPosture.Name = "toolStripBtnShowPosture";
            this.toolStripBtnShowPosture.Size = new System.Drawing.Size(92, 22);
            this.toolStripBtnShowPosture.Text = "Incarca postura";
            this.toolStripBtnShowPosture.Click += new System.EventHandler(this.toolStripBtnShowPosture_Click);
            // 
            // toolStripBtnTrain
            // 
            this.toolStripBtnTrain.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripBtnTrain.Image = ((System.Drawing.Image)(resources.GetObject("toolStripBtnTrain.Image")));
            this.toolStripBtnTrain.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripBtnTrain.Name = "toolStripBtnTrain";
            this.toolStripBtnTrain.Size = new System.Drawing.Size(70, 22);
            this.toolStripBtnTrain.Text = "Antreneaza";
            this.toolStripBtnTrain.Click += new System.EventHandler(this.toolStripBtnTrain_Click);
            // 
            // toolStripSeparator3
            // 
            this.toolStripSeparator3.Name = "toolStripSeparator3";
            this.toolStripSeparator3.Size = new System.Drawing.Size(6, 25);
            // 
            // toolStripButtonClassify
            // 
            this.toolStripButtonClassify.DisplayStyle = System.Windows.Forms.ToolStripItemDisplayStyle.Text;
            this.toolStripButtonClassify.Image = ((System.Drawing.Image)(resources.GetObject("toolStripButtonClassify.Image")));
            this.toolStripButtonClassify.ImageTransparentColor = System.Drawing.Color.Magenta;
            this.toolStripButtonClassify.Name = "toolStripButtonClassify";
            this.toolStripButtonClassify.Size = new System.Drawing.Size(55, 22);
            this.toolStripButtonClassify.Text = "Clasifica";
            this.toolStripButtonClassify.Click += new System.EventHandler(this.toolStripButtonClassify_Click);
            // 
            // MainForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(663, 501);
            this.Controls.Add(this.toolStrip1);
            this.Controls.Add(this.pbSkeleton);
            this.Name = "MainForm";
            this.Text = "Kinect Client";
            this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.Form1_FormClosing);
            ((System.ComponentModel.ISupportInitialize)(this.pbSkeleton)).EndInit();
            this.toolStrip1.ResumeLayout(false);
            this.toolStrip1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.PictureBox pbSkeleton;
        private System.Windows.Forms.ToolStrip toolStrip1;
        private System.Windows.Forms.ToolStripButton toolStripBtnSaveBodyPosture;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator1;
        private System.Windows.Forms.ToolStripButton toolStripBtnExit;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator2;
        private System.Windows.Forms.ToolStripButton toolStripBtnTrain;
        private System.Windows.Forms.ToolStripSeparator toolStripSeparator3;
        private System.Windows.Forms.ToolStripButton toolStripBtnShowPosture;
        private System.Windows.Forms.FolderBrowserDialog folderBrowserDialog;
        private System.Windows.Forms.ToolStripButton toolStripButtonClassify;
    }
}

