# Sample app using kinect and KNN to determine up/down positions for arms and legs

Tasks:  
0. Create dataset (a couple of postures)  
1. Train on a dataset (button)  
2. Recognize posture live when requested (new UI button)  
3. Feature to recognize posture live in continous mode  